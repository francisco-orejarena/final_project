"""
Python Programming: Final Project

Francisco Orejarena
CSIT 505
"""

# The following are the packages that were used in the project
import json # Used to read JSON files
import numpy as np # Used to help convert density measure to a logarithmic measure
import pandas as pd # Used to handle csv. files and data
import plotly.express as px # Used for choropleth creation
import plotly.io as pio # Used to specify map requirments

# function to make sure that the map is opened in the main browser used
pio.renderers.default = 'browser'

# Indian States GeoJSON opened and read
india_states = json.load(open("states_india.geojson", "r"))

# Creating a new name for the data in the GeoJSON data, and then appending it to the empty dictionary state_id_map
state_id_map = {}
for feature in india_states["features"]:
    feature["id"] = feature["properties"]["state_code"]
    state_id_map[feature["properties"]["st_nm"]] = feature["id"]

# Reading the langauge and population data by Indian states and territory and then adjusting the data.
Languages = pd.read_csv("languages_by_indian_states.csv")
Languages["Density"] = Languages["Density[a]"].apply(lambda x: int(x.split("/")[0].replace(",", "")))
Languages["id"] = Languages["State or union territory"].apply(lambda x: state_id_map[x])

# Testing the csv. file; just the first five rows
print(Languages.head())

# Checking the Density no a plot
print(Languages["Density"].plot())

# Updating density to make it logarithic for a nicer visual
Languages["DensityScale"] = np.log10(Languages["Density"])
print(Languages["DensityScale"].plot())


# Choropleth Map of the Languages of India
fig = px.choropleth(
    Languages, # the csv. file data that is used for this project
    locations="id", # Rank and state ID
    geojson=india_states, # GeoJSON file that outlines the country of India and its states in the proper shape
    color="DensityScale", # A numerical scale to make the choropleth map (required, map doesn't work otherwise)
    hover_name="State or union territory", # The state name given over the hovering label of the map
    hover_data=["Density", "Language 1", "Language 2", "Language 3", "Language 4", "Language 5"], # Displaying just the population density as well as the languages of the state in hover label
    title="India Population Density and Languages Spoken", # Title of the choropleth
)
fig.update_geos(fitbounds="locations", visible=False)
fig.show()